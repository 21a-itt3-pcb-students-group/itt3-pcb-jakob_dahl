# Summary of activities
- Added copper plane to top and bottom etch layer
- Set constraint for minimum trace width (0.5mm for VCC, 0.3mm for signals)
- Added via drill hole for every net
- Replaced Magnenometer with DHT11
- Routed board
- Added silkscreen text for DHT11 and OLED display
- Checked for DRC errors
- Generated gerber and drill files
- Gerber files checked on https://gerber-viewer.ucamco.com/

# Lessons learned (compared to the learning goals)
- How the ground plane works in a PCB
- How routing works in OrCAD PCB Designer

# Ressources used (Links, lectures etc.)
- https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/issues
- https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww45
- https://jlcpcb.com/
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-07-create-copper-plane-and-shapes
- https://www.edn.com/ten-best-practices-of-pcb-design/
- https://www.pannam.com/blog/pcb-design-guide/
- https://www.4pcb.com/trace-width-calculator.html
- https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx
- https://jlcpcb.com/capabilities/Capabilities
- https://www.infinite-electronic.dk/datasheet/d6-BM1422AGMV-ZE2.pdf
- https://www.youtube.com/watch?v=axA8QMFSrTc&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=19
- https://www.ucamco.com/en/gerber
- https://en.wikipedia.org/wiki/PCB_NC_formats
- https://gerber-viewer.ucamco.com/