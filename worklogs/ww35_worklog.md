# Summary of activities
- Read through exercise- and weekly plan
- Going to do complete needed Nordcad courses
- Draw schematic of eCompass carrier board
- Simulate eCompass carrier board

# Lessons learned (compared to the learning goals)
- N/A


# Ressources used (Links, lectures etc.)
- https://www.youtube.com/watch?v=5Ky_oswj7TI
- https://www.farnell.com/datasheets/2630938.pdf
- https://dk.farnell.com/rohm/bm1422agmv-ze2/mems-module-3-axis-magnetometer/dp/2908647
- https://www.farnell.com/datasheets/3212075.pdf
- https://dk.farnell.com/midas/mdog128032c2v-ws/oled-graphic-display-cog-128-x/dp/3759037?st=oled%20128
- https://www.farnell.com/datasheets/2706287.pdf
- https://dk.farnell.com/c-k-components/ksc631j-lfs/tact-switch-spst-no-0-05a-32vdc/dp/2908816