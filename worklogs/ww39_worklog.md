# Summary of activities
- Replaced PSpice components (Resistor and capacicators) with real components
- Looking up available components on JLCPCB
- Remember to ask question about wiring the BM1422AGMV IC in OrCad
- Create own OLED Display component, as it does not exist in the available providers in OrCAD Capture
- Create own footprint for OLED Display component


# Lessons learned (compared to the learning goals)
- Learned how to create own symbol
- Learned how to create own footprint

# Ressources used (Links, lectures etc.)
- https://eal-itt.gitlab.io/21a-itt3-pcb/weekly-plans/weekly_ww37
- https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww37
- https://jlcpcb.com/parts
- https://www.newark.com/uf-nf-pf-capacitor-conversion-table
- https://fscdn.rohm.com/en/products/databook/datasheet/ic/sensor/geomagnetic/bm1422agmv-e.pdf
- https://datasheets.raspberrypi.org/pico/Pico-R3-A4-Pinout.pdf
- https://datasheet.lcsc.com/lcsc/1810162110_YAGEO-RC0805JR-07470RL_C114747.pdf
- https://datasheet.lcsc.com/lcsc/1912201003_Samsung-Electro-Mechanics-CL31A105KB9LNNC_C444884.pdf
- https://datasheet.lcsc.com/lcsc/1810221121_Samsung-Electro-Mechanics-CL31B103KBCNNNC_C153292.pdf
- https://datasheet.lcsc.com/lcsc/1810221109_Samsung-Electro-Mechanics-CL31B104KBCNNNC_C24497.pdf