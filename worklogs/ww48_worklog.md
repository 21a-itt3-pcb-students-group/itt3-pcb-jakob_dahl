# Summary of activities
- Created rev. 2 schematic in KiCad
- Ran Electrical Rules Checker for schematic, no errors
- Imported netlist to PCB
- Created board outline
- Placed components on PCB
- Placed mounting holes
- Created GND pours/planes
- Created traces/tracks
- Ran DRC Control for PCB, no errors
- Generated drill and gerber files
- Downloaded Jacobs, Daniels, Vladimirs, Mathias's and my own PCB, and sent them to production at JLCPCB

# Lessons learned (compared to the learning goals)
- Working in KiCAD
- Difference between working in KiCad compared to OrCad (It's way easier in KiCad)
- How the process of creating a schematic in KiCad works
- How the process of creating a PCB in KiCad works

# Pro's and con's (OrCad and KiCAD)
|✔ = yes, ➗ = somewhat ❌ = no, | OrCad | KiCAD |
| - | - | - |
| Free? |❌|✔|
| Beginner friendly |❌|✔|
| intuitive layout |➗|✔|
| Software performance |❌|✔|
| Extensive configureability |✔|✔|
| Messes with your %HOME% path |✔|❌|
| Documentation and guides on how to use the software |✔|✔|
| Helpful community |➗|✔|
| Open source|❌|✔|
| Takes hours to install?|✔|❌

# Ressources used (Links, lectures etc.)
- https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/issues
- https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww48
- https://support.snapeda.com/en/articles/2651507-how-to-import-into-kicad
- https://www.youtube.com/watch?v=ia2n7P3Csac
- https://www.youtube.com/watch?v=nuC_nDz1N1s
- https://www.youtube.com/watch?v=zXYUveXtT3A
- https://www.youtube.com/watch?v=tzOvvx7UPl4
- https://forum.kicad.info/t/ground-plane-setup/16525/13
- https://www.youtube.com/watch?v=WcdJ7FAmD7k
- https://support.jlcpcb.com/article/149-how-to-generate-gerber-and-drill-files-in-kicad
- https://www.youtube.com/watch?v=XZXUQ-x0Pw0