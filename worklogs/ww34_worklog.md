# Summary of activities
- Read through exercise- and weekly plan
- Checked out circuit catalogue
- Looked up ideas of potential carrier boards
- Thought of potential sensors/actuators that could be a part of the carrier board.
- Set up GitLab repo
- Renew OrCad student license

# Lessons learned (compared to the learning goals)
- Found out what a "Particle Boron" is
- Learned about PCBs and the entire process of making them.
- What sensor to actually use for the idea of a carrier board, that would act as a compass :-)


# Ressources used (Links, lectures etc.)
- https://www.youtube.com/watch?v=efPsvz6j6ao
- www.grandideastudio.com/pizza-compass/
- https://jlcpcb.com/
- https://www.youtube.com/watch?v=w3Gz472O93s
- https://cdn-shop.adafruit.com/datasheets/HMC5883L_3-Axis_Digital_Compass_IC.pdf
