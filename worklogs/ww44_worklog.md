# Summary of activities
- Created functioning mounting holes
- Placed mounting holes
- Setting up constraints to match JLCPCBs capabilities

# Lessons learned (compared to the learning goals)
- Learned abot OrCADs environmental Settings
- Learned how to hide rats nest (Easier overview of PCB)
- Learned about DFM for PCBs

# DFM considerations
- FR-4
- electro-deposited copper foil (ED Copper)
- Make sure all parts are available and at a good price
- Find out what minimum board thickness should be
- Make sure that there is space for large components (fx. OLED Display) 

# Ressources used (Links, lectures etc.)
- https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/issues
- https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww44
- https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww45
- https://jlcpcb.com/
- https://www.youtube.com/watch?v=XBoh7U3g5TU
- http://education.ema-eda.com/iTrain/PCBEditor163/Appendix_A.html
- https://community.cadence.com/cadence_technology_forums/f/pcb-design/19922/allegro-footprint-so8nlk-was-not-found-in-the-search-path
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols
- https://www.altium.com/design-manufacturing-resources
- https://resources.pcb.cadence.com/blog/design-for-manufacturing-or-dfm-analysis-pcb-dfm-process-slp
- https://jlcpcb.com/capabilities/Capabilities
