# Summary of activities
- Read through exercise- and weekly plan
- Completed courses on Nordcad
- Completed schematic of eCompass carrier board

# Lessons learned (compared to the learning goals)
- Got alot more experience on reading datasheets
- More experience in using OrCad Capture CIS
- Learnt more about symbols and footprints
- Learnt about decoupling capacictors


# Ressources used (Links, lectures etc.)
- https://datasheets.raspberrypi.org/pico/Pico-R3-A4-Pinout.pdf
- https://www.newhavendisplay.com/specs/NHD-1.27-12896UGC3.pdf
- https://www.youtube.com/watch?v=PCHahR7jBbQ
- https://www.ultralibrarian.com/
- http://componentsearchengine.com/
- https://octopart.com/
- https://www.electronics-notes.com/articles/electronic_components/surface-mount-technology-smd-smt/packages.php
- https://www.pcbonline.com/blog/pcb-smt-components.html
- https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types#PIN-PITCH
- https://en.wikipedia.org/wiki/Surface-mount_technology
