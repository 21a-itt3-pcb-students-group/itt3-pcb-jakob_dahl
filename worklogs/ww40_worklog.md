# Summary of activities
- Finishing up OLED Display footprint
- Working on padstack
- Assigned Pin number to OLED Display symbol
- Updated PCB Board layout
- Netlist generated
- Created board outline
- Created own mounting hole
- Placed components in layout
- Added picture of footprints to spreadsheet of components

# Lessons learned (compared to the learning goals)
- Learned more about padstacks
- Learned more about footprint (Creating one, assigning it to existing symbol and how padstacks are used for footprints)
- Learned more about the process of designing the PCB in PCB Editor/Designer (Netlist, Board outline and mounting holes fx. used in enclosures)

# Ressources used (Links, lectures etc.)
- https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww37
- https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/issues
- https://www.nordcad.dk/student-forum/onlinekursus/
- https://jlcpcb.com/
- https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-02-generate-a-pcb-editor-netlist-in-orcad-capture