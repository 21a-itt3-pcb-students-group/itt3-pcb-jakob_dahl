# Summary of activities
- As of today i will make sure to add every exercise to my GitLab board to keep track of what needs to be done. I will also update indiviual issues with the current state of them.
- Do Design Rule Check (DRC)
- Make Netlist (Transfer schematic and footprints to Orcad PCB Editor)
- Make board outline
- Create mounting holes


# Lessons learned (compared to the learning goals)
- 

# Ressources used (Links, lectures etc.)
- https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl