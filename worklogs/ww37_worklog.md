# Summary of activities
- Read through exercise- and weekly plan
- Researched components for the carrier board
- Researched different components (Passive rectangular components, Transistor and diode packages and Integrated circuit SMD packages)
- 

# Lessons learned (compared to the learning goals)
- Learned more about capacicators (The unit Farad etc.)
- Learned more about footprints

# Ressources used (Links, lectures etc.)
- https://www.mouser.dk
- https://jlcpcb.com/parts
- https://www.az-delivery.de/en/products/0-96zolldisplay
- https://en.wikipedia.org/wiki/Farad
- https://www.mouser.dk/ProductDetail/Parallax/28085?qs=%2FPVulymFwT2bZ0ufqCM6wQ==
- https://www.mouser.dk/ProductDetail/Eagle-Plastic-Devices/101-TS6824T1601-EV?qs=wZ6BIqq6v%252BmBV6pgL6xTkg==
- https://uk.rs-online.com/web/generalDisplay.html?id=ideas-and-advice/oled-displays-guide
- https://en.wikipedia.org/wiki/Reference_designator
- https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types
- https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww36