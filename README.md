# ITT3-PCB-Jakob_Dahl
### RPi Pico IoT carrier board - DHT11 & OLED Display

## To-Do
#### Week 34 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww34_worklog.md)
- [X] Circuit research
- [X] Setup gitlab documentation project
- [X] Project log
- [X] OrCad installation and license
#### Week 35 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww35_worklog.md)
- [X] Nordcad beginners course
- [X] Draw schematic
- [ ] Simulation
#### Week 36 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww36_worklog.md)
- [X] SMT component packages
- [X] Component research
- [X] Footprints
#### Week 37 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww37_worklog.md)
- [X] Design rule check
- [X] Netlist
- [X] Board outline
- [X] Create mounting holes
#### Week 38 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww38_worklog.md)
- [X] Nordcad workshop
#### Week 39 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww39_worklog.md)
- [X] Design for manufacturing workshop
#### Week 40 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww40_worklog.md)
#### Week 44 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww44_worklog.md)
- [X] Component placement
- [X] Design guidelines for Successful Manufacturing
- [X] Setup constraints in ORcad PCB editor
- [X] Copper plane and shapes
#### Week 45 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww45_worklog.md) 
- [X] Route the board
- [X] Silkscreen
- [X] Clear DRC errors
- [ ] Design sanity check
- [X] Create Gerber and Drill files
- [X] Inspect gerbers and drill file
- [ ] Order from JLC PCB
#### Week 46 - [Worklog](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/worklogs/ww46_worklog.md)
- [X] Course exercises overview
- [X] Documentation

## Links
- [Worklogs](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/tree/main/worklogs)
- [Block Diagram](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/documentation/block_diagram.png)
- [Schematic](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/documentation/schematic.pdf)
- [Components w/ footprints & BOM](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/documentation/componentsV3.pdf)
- [Simulation](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/documentation/simulation.xlsx)
- [Gerber & Drill file](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/documentation/gerber_drill_files.rar)
- [Stackup](https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-jakob_dahl/-/blob/main/documentation/stackup.png)