.ALIASES
V_V1            V1(+=VCC -=GND ) CN @RP2040_ECOMPASS_CARRIER_BOARD.SCHEMATIC1(sch_1):INS14926@SOURCE.VDC.Normal(chips)
_    _(GND=GND)
_    _(GPIO10=GPIO10)
_    _(GPIO11=GPIO11)
_    _(GPIO12=GPIO12)
_    _(GPIO16=GPIO16)
_    _(GPIO18=GPIO18)
_    _(GPIO22=GPIO22)
_    _(SCLK1=SCLK1)
_    _(SCLK2=SCLK2)
_    _(SDA1=SDA1)
_    _(SDA2=SDA2)
_    _(VCC=VCC)
.ENDALIASES
